# handbook


## Guide for Distribuite Work


* [Remote Working Guide](remote_working_guides/remote_working_guide.md)
* [Flexible Working Guide](remote_working_guides/flexible_working_guide.md)


## Docker images Snippets

* [Postgresql](docker/postgres.md)
* [Using Amazon Aws ECR](docker/aws_ecr.md)


## Micronaut

* [Micronaut Generic Guide](micronaut/index.md)


## Quarkus



## UseFull Links

[Git Cheat Sheet](https://education.github.com/git-cheat-sheet-education.pdf)

[Jet Brains](https://resources.jetbrains.com/storage/products/intellij-idea/docs/IntelliJIDEA_ReferenceCard.pdf)