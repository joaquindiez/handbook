
## Introduction to flexible working

Flexible working is a great company perk, stated in our core culture values and everyone in Clarity should be able to benefit from it.

There is a common misconception that flexible working means part-time working. Whilst it can include part-time working, it does not have to, as it’s much broader than that. It can encompass any change to working hours. The options are endless including compressed hours, term-time working, flexi-time, job sharing to name a few.

The long list of potential benefits include: 

* a more efficient and productive organisation; 
* a more empowered and motivated workforce; 
* better customer service and increased customer loyalty; 
* increased staff retention and attractiveness to potential employees;
* reduced levels of sickness absence; 
* and working hours that best suit your employees and customers.



## Tip, Tricks and caveats



* **Be Organised**: as you might not be returning to the same location every day you need to be very organised with your files. You may need to carry the documents and resources you need with you so if possible file as much electronically.  Dump the junk and get rid of all unnecessary clutter.  The last thing you want to do is be carrying around heavy bags! There are also security aspects to consider, making sure documents are saved in the right locations so they are secure and backed up.
* **Be Engaged**: one of the problems with not being sat next to the same person every day is that you can feel isolated and lose connectivity with the rest of your team. Keep in regular contact with your team, plan face to face meetings in the office, sit with them as often as you can, go to team social events but most important of all if you can’t interact face to face, use the phone not email wherever possible 
* **Be Equipped**: to maximize your ability to work flexibly you will need to have the right equipment and make proper use of it. Understand what your organisation is making available (both hardware and software) and learn how to use it.  You really must attend any training offered as it will be invaluable in helping you get the most out of any resources. Learn how to access your files remotely from wherever you are and learn what new software is being made available. 
* **Be Visible**: Don’t be tempted to hide yourself away too much. If you want to be trusted to self manage and get on with your work in a flexible way then earn that trust by communicating regularly, agreeing your outputs and delivering on them. The bonus is not just on the employee, if you are a manager the you equally must learn to trust your employees and learn to manage them by output.
* **Be Productive**: it’s very easy when working on your own to fritter the day away doing “stuff”.By “stuff” we mean tasks and activities that make you feel busy but do not contribute to the critical things you need to get done that day.
* **Be Focused**: Focus on the output that is required of you. Make sure you understand from your line manager what is expected and by when. This has to be your priority and will be the main way your performance will be measured.
* **Be Aware**: Be aware of when you are at your most productive. Everybody is different and works in a different way. Some people genuinely are “morning” people and others “afternoon” people or even “night” (up to you). Flexible working allows you to plan your day to do the more focussed tasks when you are at your peak. You will be amazed about how much more you get done by listening to your body and understanding it’s energy cycles.
* **Be Flexible**: Is all about flexibility but it only works if all your colleagues are equally flexible and considerate to each other.
* **Be honest and realistic**: about what work can be done flexibly and consider where, when and how it is to be done. Identify and publicise senior role models working flexibly to ensure flexible work is a viable choice and not seen to be career limiting
* **Publish your State**: your team mates need to know when are a available, ***block the calendar*** when you are not going to be working, or when you are not going to answer, so the rest of your coworkers can know when to count on you
* **Add a Photo**: you team mates may have never see you, so it is nice and friendly you add a photo of yourself so every can put a face to who are talking to. That means Slack, Google Mail.

