
## Introduction to Remote Working


Working at home, though, requires discipline and autonomy. Also, not everyone really enjoy this way of working. 

This is just part of being in a diverse team.

If you are interested in being a better and more efficient remote worker, keep reading!

**As an employer,** restricting your hiring to a small geographic region means you’re not getting the best people you can. 

**As an employee,** restricting your job search to companies within a reasonable commute means you’re not working for the best company you can. REMOTE shows both employers and employees how they can work together, remotely, from any desk, in any place, anytime, anywhere.

This is a quote from a book called **REMOTE: Office Not Required** - very recommended if you’re eager to explore a whole new world of useful tips and facts you (maybe) didn’t know!

Some of the most fun and informative essays in the book are:

* Why work doesn’t happen at work
* Stop commuting your life away
* It’s the technology, stupid
* Escaping 9am-5pm
* Magic only happens when we’re all in a room
* If I can’t see them, how do I know they are working?
* Cabin fever

I would really encourage everyone interested in the topic to read the whole book. This guide from Trello is also a great read!

As we stated before, not everyone enjoys this way of working.

 You probably know already many people that prefer to go to an office, socialize, have some after-work drinks with some teammates, and you also probably know people that are quite the opposite.

A very diverse set of people lead to a very diverse set of things to take care of when you are not working: kids, older family members to take care of, pets, significant others, hobbies in general.

This is where working remotely can make a difference.


## How to work remotely: Tips, Tricks and caveats


* **Work from anywhere you want:**

Working remotely allows you to be there for the ones you love, and be more available for them. It allows you to see more places, without ever having to commute to work.
Over-communicate: Remote people have to excel when it comes to communication. Luckily, technology is on our side, so you need to make an efficient use of tools like Slack, Google Hangouts, Google Docs, etc.

* **Communication (again)**

    * Use asynchronous communication when possible (issues and email instead of chat)
Despite the instantaneous nature of chat, it should be considered asynchronous communication. Don’t expect an instantaneous response - you don’t know what the other person is doing
    * Sometimes using synchronous communication is the best choice, but do not stick to it as the default
    * Try to ask any kind of question in a public way, so everyone can see it and contribute accordingly
    * If you refer to something else during a conversation, try to include a link to that website/document/issue
    * JIRA issues are preferred over email, email is preferred over chat, announcements happen on the Bi-weekly team call
    * Give constructive feedback over a video call. Communication via chat can be easily misinterpreted, remember that at the end of that chat there is a human being with feelings and reactions
    * Meetings are always held over video conferences (unless all meeting members are present in the office).
    * It is important to make sure that remote people can hear everyone: try to get closer to the microphone when it’s your turn to speak, avoid simultaneous conversations too. On the other hand, remote people should be muted unless you’re speaking, in order to avoid generating extra noise
    * When using the chat, if you need to reach more than 3 people for a specific topic, create a channel in Slack for #whatever-topic instead

* **Workspace**

    * Have a designated area to do your work to avoid distractions
        * Keep yourself hydrated: always have a big bottle of water on your desk
        * Have a clean desk with only the things you need to do your work
    * Have a schedule for work and non-work stuff
        * Including:
            * Short breaks
            * Picking up your kids from school (or carrying them)
            * Taking your dogs out for a walk
            * Going to get some exercise done
            * Groceries
            * Cinema
            * Lunch, dinner
            * Meet some friends
        * Seriously, reduce distractions
            * Try to keep your discipline up and running
* **Caveats**
    * “Remote workers are slackers”
Any worker, regardless of location, can slack off if managers are not properly communicating expectations and deadlines. Make sure all your employees (not only remote ones) have clear expectations about the goals they need to complete and the deadlines. After that, everyone will be able to organize their own work-life balance accordingly
    * “Remote workers have to constantly prove that they are working”
        * Remote workers are getting up and going to work every day just like everyone else, but probably using a shorter commute time
    * “Remote work means company culture suffers”
        * Make sure you set-up team-building activities where co-workers talk about the same things they would talk in an office
            * What they did on the weekend
            * Vacations
            * Conversations about specific topics: video-games, TV, movies, etc
    * “Remote workers are 24x7 available”
        * It is always easy to assume that because someone is working from home, they are always available to answer a quick question. This is wrong. Remember work-life separation is even possible at home so don’t assume these kind of situations

